// Retrieve an element from the webpage
// console.log(document);
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// "document" refers to the whole webpage.
// "querySelector" is used to select a specific object (HTML element) form the document.

/*
Alternatively, we can use getElement functions to retrieve the elements.
document.getElementById("id")
document.getElementByClassName("class-name")
document.getElementByTagName("tag-name")
*/

function combineName(firstName, lastName){
	return `${firstName} ${lastName}`;
}

// Performs an action when an event triggers
txtFirstName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = combineName(txtFirstName.value, txtLastName.value);
	/*console.log(event.target);
	console.log(event.target.value);*/
}); // end txtFirstName.addEventListener

txtLastName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = combineName(txtFirstName.value, txtLastName.value);
	/*console.log(event.target);
	console.log(event.target.value);*/
}); // end txtFirstName.addEventListener